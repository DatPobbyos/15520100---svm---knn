from sklearn import svm
from sklearn.externals import joblib
from sklearn.neighbors import KNeighborsClassifier
import numpy as np
import os
name_class = []
def load_features(src):
    print("[+] Load data....")
    data = []
    target = []
    key = 0
    for folder in os.listdir(src):
        folder_path = os.path.join(src, folder)
        name_class.append(folder)        
        for file in os.listdir(folder_path):
            target.append(key)
            data.append(np.load(os.path.join(folder_path, file))[0])
        key = key+1
    print("[+] Load data finished")
    return data,target
def svmtrain(data,target):
    clf = svm.LinearSVC(random_state=0, tol=1e-5)
    clf.fit(data, target)
    return clf
def savetraindb(clf,name):
    save_path_ = os.path.join('exp/svmlinear', name)
    os.makedirs(save_path_)
    name_path = os.path.join(save_path_, 'name.npy')
    np.save(name_path, name_class)
    file_name = "clf" + ".joblib"
    save_path = os.path.join(save_path_, file_name)    
    print("[+] Saving model to file : " ,file_name)
    joblib.dump(clf, save_path)
def main():
    src = 'features/vgg16_fc2'
    data,target = load_features(src)
    print ('training...')
    clf = svmtrain(data,target)
    name = input('Enter db name: ')
    savetraindb(clf,name)
if __name__=='__main__':
    main()