from sklearn import svm
from sklearn.externals import joblib
import numpy as np
import os
name_class = []
def predict_test(clf,data,target,test_path,src):
    print ("Testing...")
    result =[]
    sum = 0
    for (key,vector) in enumerate(data):
        temp =clf.predict(vector)
        if target[key] == temp[0]:
            sum +=1
        result.append(temp[0])
    accuracy = float((sum/len(target))*100)
    src_ = os.path.join(src,'result.txt')
    name_train = np.load(os.path.join(src,'name.npy'))
    with open(src_,"w") as f:
        f.write("Accuracy:\t"+str(accuracy)+"\n")
        for (key,value) in enumerate(result):
            f.write("[+]Test Data:\t"+test_path[key]+"\n")
            f.write("\t[-]Class:\t"+name_class[target[key]]+"\n")
            f.write("\t[-]Predict:\t"+name_train[value]+"\n")
def load_test(src):
    print ("Read test data")
    data_path = []
    data = []
    target = []
    key = 0
    for dir_ in os.listdir(src):
        dir_path = os.path.join(src,dir_)
        name_class.append(dir_)
        for file in os.listdir(dir_path):
            file_path = os.path.join(dir_path,file)
            data.append(np.load(file_path))
            target.append(key)
            data_path.append(file_path)
        key +=1
    return data,target,data_path
def load_train(src):
    return joblib.load(src)
def main():
    src_test = 'features/test/vgg16_fc2'
    test_data,test_target,test_path = load_test(src_test)
    src = input('Enter db path: ')
    src_train = os.path.join(src,'clf.joblib')
    clf = joblib.load(src_train)
    predict_test(clf,test_data,test_target,test_path,src)
if __name__=='__main__':
    main()
