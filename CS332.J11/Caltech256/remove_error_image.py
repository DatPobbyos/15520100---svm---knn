from keras.preprocessing import image
import os
import shutil
from PIL import ImageFile

ImageFile.LOAD_TRUNCATED_IMAGES=True

def removeImageCantRead(src):
    for dir in os.listdir(src):
        dir_ = os.path.join(src,dir)
        for file in os.listdir(dir_):
            img_path = os.path.join(dir_,file)
            save_path = img_path.replace("images", "errorImages")
            print ("(+)Try read image: ",img_path)
            try:
                image.load_img(img_path, target_size=(224, 224))
            except OSError:
                # print ('Cannot open')
                dir_save = dir_.replace("images", "errorImages")
                if not os.path.isdir(dir_save):
                    os.makedirs(dir_save)
                print ('\tMove:',img_path,"\tTo\t",save_path)
                shutil.move(img_path, save_path)
            # else:
            #     print ('Can open')
def main():
    src = "images"
    removeImageCantRead(src)
    print('Proccess done')

if __name__=="__main__":
    main()    
